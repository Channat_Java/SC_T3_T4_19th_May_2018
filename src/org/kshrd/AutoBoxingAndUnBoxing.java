package org.kshrd;

public class AutoBoxingAndUnBoxing {

    public static void main(String[] args) {

        Integer a = 50; // Autoboxing
        Float f = 50.0F;
        Long l = 50L;

        Integer myAge = new Integer(30);
        int age = myAge; // Unboxing


        /*
            Integer a = 50;  // 50 is int
            Double d = 50.0; // 50.0 is couble
            Float f = 50.0;

           integer (no point) that we assign is default INTEGER (primitiv)
           floating point default is DOUBLE (primitive)

         */


    }

}
