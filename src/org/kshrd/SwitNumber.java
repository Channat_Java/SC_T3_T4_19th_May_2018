package org.kshrd;

public class SwitNumber {

    // 1+2+3+...+n
    int sequencialNumber(int n) {
        int total = 0;
        for(int i=1; i<=n; i+=2) {
            total = total + i;
        }
        return total;
    }

    // (1×2×3)+(2×3×4)+.....(n(n+1)(n+2))
    int multiplySequenceNumber(int n) {
        int total = 0;
        for (int i=1; i<=n; i++) {
            total = total + (i*(i+1)*(i+2));
        }
        return total;
    }

    public static void main(String[] args) {

//        int mySwitNumber = sequencialNumber(5);
//        System.out.println(multiplySequenceNumber(2));

    }

}
