package org.kshrd;


class Node {
    int number;
    Node left;
    Node right;
    Node (int number) {
        this.number = number;
        left = null;
        right = null;
    }
}

public class BinarySearchTree {

    Node nodeRoot = null;



    void inOrder(Node nodeRoot) {
        if (nodeRoot != null) {
            inOrder(nodeRoot.left);
            System.out.println(nodeRoot.number);
            inOrder(nodeRoot.right);
        }
    }

    Node recursive(Node root, int number) {

        // check if root is start
        if (root == null) {
            root = new Node(number);
            return root;
        }

        /* if root not start then go down */

        if (number <root.number) {
            // check if key is smaller than root, so put it to left
            root.left = recursive(root.left, number);
        } else if (number > root.number) {
            // check if key is grater than root, so put it to right
            root.right = recursive(root.right, number);
        }
        /*else {
            // check if key is equal root, so put it to right
            root.right = recursive(root.right, number);
        }*/

        return root;
    }

    void insertBinarySearchTree(int number) {
        nodeRoot = recursive(nodeRoot, number);
    }

    public static void main(String[] args) {
        BinarySearchTree b = new BinarySearchTree();
        b.insertBinarySearchTree(4);
        b.insertBinarySearchTree(7);
        b.insertBinarySearchTree(6);
        b.insertBinarySearchTree(8);
        b.insertBinarySearchTree(2);
        b.insertBinarySearchTree(5);
        b.insertBinarySearchTree(3);
        //b.insertBinarySearchTree(2);
        b.inOrder(b.nodeRoot);
    }

}
