package org.kshrd;

public class PromotionAndCasting {

    public static void main(String[] args) {

        int a = 50;
        double d = a; // promotion: byte -> double
//        System.out.println(a);
//        System.out.println(d);

        double myDouble = 500;
        byte i = (byte) myDouble; // casting: double -> byte
        System.out.println(myDouble);
        System.out.println(i);




    }

}

